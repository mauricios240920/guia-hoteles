$(function(){
      $("[data-toggle='tooltip']").tooltip();
      $("[data-toggle='popover']").popover();
      $('.carousel').carousel({
          interval:2000
      });
      $("#contacto").on('show.bs.modal', function (e) {
          console.log("el modal se está mostrando, cambia de color y queda desabilitado");
            $("#contactoBtn").removeClass("btn-success");
            $("#contactoBtn").addClass("btn-secondary");
            $("#contactoBtn").prop("disabled",true);
         });
      $("#contacto").on('shown.bs.modal', function (e) {
          console.log("el modal se mostro");
      });
      $("#contacto").on('hide.bs.modal', function (e) {
          console.log("el modal se oculta");
      });
      $("#contacto").on('hidden.bs.modal', function (e) {
          console.log("el modal se oculto, cambia de color y queda habilitado");
            $("#contactoBtn").removeClass("btn-secondary");
            $("#contactoBtn").addClass("btn-success");
            $("#contactoBtn").prop("disabled",false);
      });
    
    });

